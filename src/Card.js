import React, { Component } from 'react';
import ReactDOM from 'react-dom'

class Card extends React.Component {
  render()  {

    return(
      <div className="card bg-dark text-white">
          <img className="card-img" src={this.props.img}/>
          <p className="card-text">{this.props.name}</p>
      </div>
    );
  }
}

// ReactDOM.render(
//   <Card name="Dana" />,
//   document.getElementById('root')
// );

export default Card;
