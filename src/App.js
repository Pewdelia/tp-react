import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Card from './Card.js';
import Footer from './Footer.js';

class App extends Component {
  render() {
    const champName = 'Gnar';
    const src = `https://ddragon.leagueoflegends.com/cdn/img/champion/splash/${champName}_0.jpg`;
    const cardsList = [
      <Card img="https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Gnar_0.jpg" name='Gnar'/>,
      <Card img="https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Ahri_0.jpg" name='Ahri'/>,
      <Card img="https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Kennen_0.jpg" name='Kennen'/>,
      <Card img="https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Gnar_1.jpg" name='Gnar'/>,
      <Card img="https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Kennen_5.jpg" name='Kennen'/>,
      <Card img="https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Ahri_7.jpg" name='Ahri'/>
    ];

    return (
      <div className="App">
        <header className="bg-dark">
            <nav className="navbar bg-dark">
                <span className="navbar-brand text-secondary">LOL MEMO</span>
            </nav>
        </header>
        <section className='card-columns'>
          {cardsList}
        </section>
        <Footer/>
      </div>

    );
  }
}

export default App;
